import { io, ManagerOptions, Socket, SocketOptions } from 'socket.io-client';
import { WebSocketResponse } from './interfaces';
import { EmitTimeoutError, RemoteServiceError } from './lib/error';

interface ClientOptions
{
    emitTimeout : number;
}

type ClientConstructorOptions = Partial<ManagerOptions & SocketOptions & ClientOptions>

export class WebSocketClient
{
    #client : Socket;
    #emitTimeout : number | undefined;

    constructor(uri : string, opts : ClientConstructorOptions = {
        transports: [ 'websocket' ], reconnection: true, emitTimeout: 20000
    })
    {
        this.#emitTimeout = opts.emitTimeout;
        this.#client = io(uri, opts);
    }

    #makeTimeoutPromise(timeout : number, serviceName : string, context : string, operation : string) : Promise<void>
    {
        return new Promise((_resolve, reject) =>
        {
            setTimeout(() =>
            {
                reject(new EmitTimeoutError(`Timed out calling ${ serviceName }:${ context }:${ operation }`));
            }, timeout);
        });
    }

    #makeEmitPromise(
        serviceName : string, context : string, operation : string,
        payload : Record<string, unknown>, meta ?: Record<string, unknown>,
        auth ?: string
    ) : Promise<WebSocketResponse>
    {
        return new Promise((resolve, _reject) =>
        {
            this.#client.emit('rpc', { serviceName, context, operation, payload, meta, auth }, (response) =>
            {
                resolve(response);
            });
        });
    }

    async #makeWebSocketRequest(
        serviceName : string, context : string, operation : string,
        payload : Record<string, unknown>, meta ?: Record<string, unknown>,
        auth ?: string, timeout ?: number
    )
        : Promise<WebSocketResponse>
    {
        const emitPromise = this.#makeEmitPromise(serviceName, context, operation, payload, meta, auth);
        if(timeout)
        {
            return Promise.race([
                emitPromise as any,
                this.#makeTimeoutPromise(timeout, serviceName, context, operation)
            ]);
        }
        return emitPromise;
    }

    get socket() : Socket
    {
        return this.#client;
    }

    async request<T>(
        serviceName : string, context : string, operation : string, payload : Record<string, unknown>,
        meta ?: Record<string, unknown>, auth ?: string,
        timeout : number | undefined = this.#emitTimeout
    )
        : Promise<T>
    {
        const result = await this.#makeWebSocketRequest(
            serviceName, context, operation, payload,
            meta, auth, timeout
        );

        if(result.status === 'success')
        {
            return result.response as T;
        }
        else
        {
            throw new RemoteServiceError(result.response);
        }
    }
}
