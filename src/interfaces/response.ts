export interface WebSocketResponse
{
    status : 'success' | 'failed';
    response : any;
}
