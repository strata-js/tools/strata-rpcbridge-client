export interface RemoteError
{
    code : string,
    message : string,
    name : string,
    stack : string,
    details ?: unknown,
}
