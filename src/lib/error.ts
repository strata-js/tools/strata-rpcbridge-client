import { RemoteError } from '../interfaces';

export class ClientError extends Error
{
    public code = 'CLIENT_ERROR';

    constructor(message : string, code ?: string)
    {
        super(message);
        if(code)
        {
            this.code = code;
        }
    }

    public toJSON(excludeStack = false) : Record<string, unknown>
    {
        return {
            name: this.constructor.name,
            message: this.message,
            code: this.code,
            stack: excludeStack ? null : this.stack
        };
    }
}

export class RemoteServiceError extends ClientError
{
    public code = 'MALFORMED_REQUEST_ERROR';
    public details ?: unknown;

    constructor(error : RemoteError)
    {
        super(error.message);
        this.code = error.code;
        this.name = error.name;
        this.stack = error.stack;
        this.details = error.details;
    }

    public toJSON(excludeStack = false) : Record<string, unknown>
    {
        return {
            ...super.toJSON(excludeStack),
            details: this.details
        };
    }
}

export class EmitTimeoutError extends ClientError
{
    public code = 'EMIT_TIMEOUT_ERROR';

    constructor(errorMsg : string)
    {
        super(errorMsg);
        this.code = 'WEBSOCKET_ERROR';
        this.name = this.constructor.name;
    }
}
