# Strata WebSocket RPC Bridge Client
As simple client used to make calls to a [Strata Service](https://gitlab.com/stratajs/strata) via the 
[Strata RPC Bridge Server](https://gitlab.com/strata-js/tools/strata-rpcbridge.git).

## Installation

This package is published via NPM's npm repository.

```bash
// npm
$ npm add @strata-js/rpcbridge-client

// yarn
$ yarn add @strata-js/rpcbridge-client
```

## Usage

```typescript
import { WebSocketClient } from '@strata-js/rpcbridge-client';

const client = new WebSocketClient('http://rpcbridge-server.hostname.com/endpoint');
const response = await client.request('serviceName', 'context', 'operation', {/* Payload */});
console.log(response);
```

#### Future Improvements
* HTTP Client
* Better Error Handling
* Custom Reconnect Logic
